package az.ingress.service;

import az.ingress.config.JwtService;
import az.ingress.domain.Authority;
import az.ingress.domain.Role;
import az.ingress.domain.User;
import az.ingress.dto.LoginRequest;
import az.ingress.exception.ExistsByUsernameException;
import az.ingress.exception.InvalidPasswordException;
import az.ingress.exception.UsernameNotFoundException;
import az.ingress.repository.UserRepository;
import az.ingress.response.ApiResponse;
import az.ingress.response.JwtResponse;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;

    @Autowired
    @Qualifier("kafkaCustomTemplate")
    private KafkaTemplate<String, User> kafkaTemplate;

    public ApiResponse signUp(LoginRequest loginRequest) {
        if (userRepository.existsByUsername(loginRequest.getUsername())) {
            throw  new ExistsByUsernameException();
        }

        Authority authority = new Authority();
        authority.setRole(Role.USER);
        User user = User.builder()
                .username(loginRequest.getUsername())
                .password(encoder.encode(loginRequest.getPassword()))
                .isAccountNonExpired(true)
                .isCredentialsNonExpired(true)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .authorities(Set.of(authority))
                .build();
        ProducerRecord record = new ProducerRecord<String,User>("register-topic",0,"test-1", user);
        kafkaTemplate.send(record);

//        userRepository.save(user);
        return new ApiResponse("Successfully registered");
    }

    public JwtResponse signIn(LoginRequest loginRequest) {
        User user = userRepository.findByUsername(loginRequest.getUsername())
                .orElseThrow(()-> new UsernameNotFoundException(loginRequest.getUsername()));
        boolean matches = encoder.matches(loginRequest.getPassword(), user.getPassword());
        if(matches){
            return JwtResponse.builder()
                    .jwtResponse(jwtService.issueToken(user))
                    .build();
        }else{
            throw new InvalidPasswordException();
        }

    }
}
