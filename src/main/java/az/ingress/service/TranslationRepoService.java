package az.ingress.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class TranslationRepoService {

    private final MessageSource messageSource;

    public String findByKey(String key, String lang, Object... arguments) {
        try {
            return messageSource.getMessage(key, arguments, new Locale(lang));
        } catch (NoSuchMessageException ex) {
            return "";
        }
    }
}
