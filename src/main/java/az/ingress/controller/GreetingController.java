package az.ingress.controller;

import az.ingress.aspect.HasRole;
import az.ingress.domain.Role;
import az.ingress.service.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/v1")
@Slf4j
@RequiredArgsConstructor
public class GreetingController {

    private final CourseService service;
    //For all


    @HasRole(Role.USER)
    @GetMapping("/hello/{id}")
    public ResponseEntity<String> greeting(@PathVariable String id) {
        log.info("real metod invocation used");
        return ResponseEntity.ok("Hello");
    }

    @GetMapping("/exam/{id}")
    public ResponseEntity<String> example(@PathVariable String id) {
        log.info("real metod invocation used");
        return ResponseEntity.ok("Hello");
    }


//    for authenticate user and managers
    @GetMapping("/user")
    public ResponseEntity<String> user(HttpServletRequest httpServletRequest) {
//        service.getCourses();
        return ResponseEntity.ok("welcome to the site");
    }

    //    for authenticate  managers
    @GetMapping("/manager")
    public ResponseEntity<String> manager() {
        return ResponseEntity.ok("welcome to the site with role  manager");
    }

}
