package az.ingress.controller;

import az.ingress.aspect.Counter;
import az.ingress.dto.LoginRequest;
import az.ingress.response.ApiResponse;
import az.ingress.response.JwtResponse;
import az.ingress.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v2")
@Counter
public class AuthController {

    private final AuthService authService;

    @PostMapping("/sign-up")
    public ApiResponse singUp(@RequestBody LoginRequest loginRequest) {
           return authService.signUp(loginRequest);
    }

    @PostMapping("/sign-in")
    public JwtResponse singIn(@RequestBody LoginRequest loginRequest) {
        return  authService.signIn(loginRequest);
    }
}
