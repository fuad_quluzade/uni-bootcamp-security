package az.ingress.extraconfig;

import az.ingress.exception.ErrorResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;

import static az.ingress.exception.UniBootcampErrorCode.EXPIRED_JWT;
import static az.ingress.exception.UniBootcampErrorCode.INVALID_CREDENTIALS;

@Component("customAuthenticationEntryPoint")
@Slf4j
public class AuthenticationEntryPointConfigurer implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException exception) throws IOException {
        log.info(exception.toString());
        final String expired = (String) request.getAttribute("expired");
        errorHandle(expired,request,response);
    }

    private void errorHandle(String result, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ErrorResponseDto re = null;
        if (result !=null && result.startsWith("JWT expired")) {
            re = ErrorResponseDto.builder()
                    .status(400)
                    .code(EXPIRED_JWT.code)
                    .path(request.getServletPath())
                    .timeStamp(OffsetDateTime.now())
                    .message("bad request")
                    .details("invalid credentials")
                    .build();
        } else  {
            re = ErrorResponseDto.builder()
                    .status(400)
                    .code(INVALID_CREDENTIALS.code)
                    .path(request.getServletPath())
                    .timeStamp(OffsetDateTime.now())
                    .message("bad request")
                    .details("invalid credentials")
                    .build();
        }

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        OutputStream responseStream = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(responseStream, re);
        responseStream.flush();
    }
}
