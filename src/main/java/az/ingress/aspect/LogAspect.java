package az.ingress.aspect;

import az.ingress.response.ApiResponse;
import az.ingress.response.JwtResponse;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LogAspect {

     @Before("execution(* az.ingress.controller.AuthController.*(..))" +
             "&& args(dto,..)")
     public void before(JoinPoint joinPoint,Object dto) throws Throwable{
          log.info("ASPECT :method start {} request - {}",joinPoint.getSignature().getName(),dto);
     }

     @AfterReturning(value = "execution(* az.ingress.controller.AuthController.*(..))" ,
             returning="response")
     public void signIN(JoinPoint joinPoint, Object response) throws Throwable{
          log.info("ASPECT :method finish {} response - {} ",joinPoint.getSignature().getName() ,response);
     }

//     @AfterReturning(value = "execution(* az.ingress.controller.AuthController.singIn(..))" ,
//             returning="response")
//     public void signIN(JoinPoint joinPoint, JwtResponse response) throws Throwable{
//          log.info("ASPECT :method finish {} response {} ",joinPoint.getSignature().getName() ,response);
//     }
//
//     @AfterReturning(value = "execution(* az.ingress.controller.AuthController.singUp(..))" ,
//             returning="response")
//     public void signUP(JoinPoint joinPoint, ApiResponse response) throws Throwable{
//          log.info("ASPECT :method finish {} response {} ",joinPoint.getSignature().getName() ,response);
//     }
}
