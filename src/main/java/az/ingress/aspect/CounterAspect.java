package az.ingress.aspect;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class CounterAspect {
    private final RedisTemplate<String, Integer> redisTemplate;
    private Integer counterSuccess = 0;
    private Integer counterException = 0;


    @AfterReturning("@within(az.ingress.aspect.Counter) || @annotation(az.ingress.aspect.Counter)")
    public void countSuccess(JoinPoint joinPoint) {
        if (Boolean.FALSE.equals(redisTemplate.hasKey(joinPoint.getSignature().getName().concat("Success")))) {
            counterSuccess = 0;
        }
        counterSuccess++;
        redisTemplate.opsForValue().set(joinPoint.getSignature().getName().concat("Success"), counterSuccess);
        System.out.println(redisTemplate.opsForValue().get(joinPoint.getSignature().getName().concat("Success")) + " : " + joinPoint.getSignature().getName());
    }
    @AfterThrowing("@within(az.ingress.aspect.Counter) || @annotation(az.ingress.aspect.Counter)")
    public void countException(JoinPoint joinPoint) {
        if (Boolean.FALSE.equals(redisTemplate.hasKey(joinPoint.getSignature().getName().concat("Exception")))) {
            counterException = 0;
        }
        counterException++;
        redisTemplate.opsForValue().set(joinPoint.getSignature().getName().concat("Exception"), counterException);
        System.out.println(redisTemplate.opsForValue().get(joinPoint.getSignature().getName().concat("Exception")) + " : " + joinPoint.getSignature().getName());
    }
}