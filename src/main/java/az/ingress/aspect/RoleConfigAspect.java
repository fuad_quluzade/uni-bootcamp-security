package az.ingress.aspect;

import az.ingress.config.SecurityService;
import az.ingress.exception.NotAllowedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
@Order(0)
public class RoleConfigAspect {

    private final SecurityService securityService;

    @Around("@annotation(az.ingress.aspect.HasRole)")
    public Object checkRole(ProceedingJoinPoint joinPoint) throws Throwable {
        var signature = (MethodSignature) joinPoint.getSignature(); // signatureni gotururem,metod siqnature eye bir class qayidir
        var annotation = signature.getMethod().getAnnotation(HasRole.class);//ve onun icinnen getannotason deyende bu annotasiyani qaytarir
        if (!securityService.getCurrentJwtCredentials().getRole().contains(annotation.value())) {
            log.debug("Exception occurs when trying to execute method {}", signature.getMethod());
            throw new NotAllowedException();
        }
        return joinPoint.proceed();
    }


    @Around("publicMethod() && @within(ann)")
    public Object checkEndpointTypeByClass(ProceedingJoinPoint joinPoint,HasRole ann) throws Throwable {
        if (!securityService.getCurrentJwtCredentials().getRole().contains(ann.value())) {
            log.debug("Exception occurs when trying to execute method {}", joinPoint.getSignature().getName());
            throw new NotAllowedException();
        }
        return joinPoint.proceed();
    }

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {
    }
}
