package az.ingress.exception;

import static az.ingress.exception.UniBootcampErrorCode.NOT_ALLOWED;

public class NotAllowedException extends UniBootCampGenericException {
    public NotAllowedException(Object... arguments) {
        super(NOT_ALLOWED.code, NOT_ALLOWED.code,400,arguments);
    }


}
