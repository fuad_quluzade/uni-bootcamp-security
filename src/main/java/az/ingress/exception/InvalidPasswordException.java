package az.ingress.exception;

import static az.ingress.exception.UniBootcampErrorCode.INVALID_PASSWORD;

public class InvalidPasswordException extends UniBootCampGenericException{

    public InvalidPasswordException(Object... arguments) {
        super(INVALID_PASSWORD.code, INVALID_PASSWORD.code, 400, arguments);
    }
}
