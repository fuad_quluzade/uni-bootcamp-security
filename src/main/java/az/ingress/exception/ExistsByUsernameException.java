package az.ingress.exception;

import static az.ingress.exception.UniBootcampErrorCode.EXISTS_BY_USERNAME;

public class ExistsByUsernameException extends UniBootCampGenericException{
    public ExistsByUsernameException(Object... arguments) {
        super(EXISTS_BY_USERNAME.code, EXISTS_BY_USERNAME.code, 404, arguments);
    }
}
