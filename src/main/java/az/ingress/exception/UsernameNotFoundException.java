package az.ingress.exception;

import org.springframework.http.HttpStatus;

import static az.ingress.exception.UniBootcampErrorCode.USER_NOT_FOUND;

public class UsernameNotFoundException extends UniBootCampGenericException {
    public UsernameNotFoundException(Object... arguments) {
        super(USER_NOT_FOUND.code, USER_NOT_FOUND.code,404,arguments);
    }

//    public UsernameNotFoundException(String errorBody, HttpStatus statusCode) {
//        super(errorBody, statusCode);
//    }
}
