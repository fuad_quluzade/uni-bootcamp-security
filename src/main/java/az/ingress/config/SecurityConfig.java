package az.ingress.config;

import az.ingress.extraconfig.AuthenticationEntryPointConfigurer;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthFilterConfigurerAdapter jwtAuthFilterConfigurerAdapter;

    private final AuthenticationEntryPointConfigurer configurer;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().disable();
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests()
                .antMatchers("/v2/**").permitAll()
        .antMatchers(GET,"/v1/user").hasAuthority("USER")
        .antMatchers(POST,"/v1/user").hasAuthority("MANAGER")
        .antMatchers("/v1/manager").hasRole("MANAGER")
        .antMatchers("/v1/hello/**").permitAll();
        http.formLogin().defaultSuccessUrl("https://turbo.az");
        http.apply(jwtAuthFilterConfigurerAdapter);

        http.exceptionHandling().authenticationEntryPoint(configurer);
        super.configure(http);
    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("user")
//                .password("{noop}10")
//                .roles("USER")
//                .and()
//                .withUser("manager")
//                .password("{noop}20")
//                .roles("MANAGER");
//
//        super.configure(auth);
//    }
}
