package az.ingress;

import az.ingress.domain.Authority;
import az.ingress.domain.Role;
import az.ingress.domain.User;
import az.ingress.repository.UserRepository;
import az.ingress.config.JwtService;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class UniBootcampSecurityApplication implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;

    public static void main(String[] args) {
        SpringApplication.run(UniBootcampSecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//
//        Authority authority=new Authority();
//        authority.setRole(Role.USER);
//        User user= User.builder()
//                .username("Abbas")
//                .password(encoder.encode("102030"))
//                .isAccountNonExpired(true)
//                .isCredentialsNonExpired(true)
//                .isEnabled(true)
//                .isAccountNonLocked(true)
//                .authorities(Set.of(authority))
//                .build();
//
//        userRepository.save(user);
//        log.info("user save" +user);
//
//        String s =jwtService.issueToken(user);
//
//        log.info("token generated " + s);
//
//        Claims claims  = jwtService.parseToken(s);
//        log.info("token parsed . data is {}" +claims);
    }
}
